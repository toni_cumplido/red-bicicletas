var mongoose = require('mongoose');
var Bicicleta = require("../../models/bicicleta");
var request = require("request");
var server = require("../../bin/www");


var base_url = "http://localhost:5000/api/bicicletas";

describe('Testing Bicicletas', function(){
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection_error'));
        db.once('open', function() {
            console.log('We are connected to test database!');
            done();
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();
        });
    });

    describe("GET BICICLETAS /", () => {
        it("Status 200", (done) => {
            request.get(base_url, function(error, response, body){
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.Bicicleta.length).toBe(0);
            });
        });
    });
    describe("POST BICICLETAS /create", () => {
        it("STATUS 200", (done) => {
            var headers = {"content-type" : "application/json"};
            var aBici = '{ "code" : 10 , "color" : "rojo", "modelo" : "urbana" , [ 51.50137, -0.14174900000000656] }';               
            request.post({
                headers:    headers,
                url:        base_url + '/create',
                body:       aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.color).toBe("rojo");
                expect(bici.ubicacion[0]).toBe(51.50137);
                expect(bici.ubicacion[1].toBe(-0.14174900000000656);
                done();
            });
        });
    });
    
    describe("DELETE BICICLETAS /delete", () => {
        it("STATUS 204", (done) => {
            var a = Bicicleta.createInstance(1, 'negro', 'urbana', [ 51.50137,-0.14174900000000656]);
            Bicicleta.addListener(a, function(err, newBici){
                var headers = {"content-type" : "application/json"};
                var aBici = '{ "code" : 1 , "color" : "rojo", "modelo" : "urbana" , "lat" : 51.50137, "lng" : -0.14174900000000656 }';               
                request.post({
                    headers:    headers,
                    url:        base_url + '/create',
                    body:       aBici
                }, function(error, response, body) {
                    expect(response.statusCode).toBe(200);
                    var bici = JSON.parse(body).bicicleta;
                    console.log(bici);
                    expect(bici.color).toBe("rojo");
    
                    var aBici = '{ "id" : 1 }';               
                    request.delete({
                        headers:    headers,
                        url:        base_url + '/delete',
                        body:       aBici
                    }, function(error, response, body) {
                        expect(response.statusCode).toBe(204);
                        done();
                    });
                });

            });
                
        });
    });



});

/*
describe("Bicicleta API", () => {
    describe("GET BICICLETAS /", () => {
        it("Status 200", () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1 , 'rojo', 'urbana', [51.50137,-0.14174900000000656]);               
            Bicicleta.add(a);

            request.get("http://localhost:5000/api/bicicletas", function(error, response, body){
                expect(response.statusCode).toBe(200);
            });
        });
    });
    describe("POST BICICLETAS /create", () => {
        it("STATUS 200", (done) => {
            var headers = {"content-type" : "application/json"};
            var aBici = '{ "id" : 1 , "color" : "rojo", "modelo" : "urbana" , "lat" : 51.50137, "lng" : -0.14174900000000656 }';               
            request.post({
                headers:    headers,
                url:        "http://localhost:5000/api/bicicletas/create",
                body:       aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(1).color).toBe("rojo");
                done();
            });
        });
    });
    describe("POST BICICLETAS /update", () => {
        it("STATUS 200", (done) => {
            var headers = {"content-type" : "application/json"};
            var aBici = '{ "id" : 1 , "color" : "rojo", "modelo" : "urbana" , "lat" : 51.50137, "lng" : -0.14174900000000656 }';               
            request.post({
                headers:    headers,
                url:        "http://localhost:5000/api/bicicletas/create",
                body:       aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(1).color).toBe("rojo");

                var aBici = '{ "id" : 1 , "color" : "azul", "modelo" : "urbana" , "lat" : 51.50137, "lng" : -0.14174900000000656 }';               
                request.post({
                    headers:    headers,
                    url:        "http://localhost:5000/api/bicicletas/update",
                    body:       aBici
                }, function(error, response, body) {
                    expect(response.statusCode).toBe(200);
                    expect(Bicicleta.findById(1).color).toBe("azul");
                    done();
                });
            });            
        });
    });
    describe("DELETE BICICLETAS /delete", () => {
        it("STATUS 204", (done) => {
            var headers = {"content-type" : "application/json"};
            var aBici = '{ "id" : 1 , "color" : "rojo", "modelo" : "urbana" , "lat" : 51.50137, "lng" : -0.14174900000000656 }';               
            request.post({
                headers:    headers,
                url:        "http://localhost:5000/api/bicicletas/create",
                body:       aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(1).color).toBe("rojo");

                var aBici = '{ "id" : 1 }';               
                request.delete({
                    headers:    headers,
                    url:        "http://localhost:5000/api/bicicletas/delete",
                    body:       aBici
                }, function(error, response, body) {
                    expect(response.statusCode).toBe(204);
                    done();
                });
            });    
        });
    });


});

