var mymap = L.map('main_map').setView([51.505, -0.09], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',    
}).addTo(mymap);

/*
L.marker([51.50075500000001,-0.12466899999999059]).addTo(mymap); //BIG BEN
L.marker([51.50137,-0.14174900000000656]).addTo(mymap);  // BUCKINGHAM PALACE
L.marker([51.470036114193974,0.007060000000014277]).addTo(mymap); // OBSERVATORIO GREENWICH
*/

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function (bici){
            L.marker(bici.ubicacion, {title: bici.id }).addTo(mymap);
        });
    }
});